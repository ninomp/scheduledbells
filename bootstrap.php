<?php
define('APP_ROOT_DIR', dirname(__FILE__));

set_include_path(get_include_path() . PATH_SEPARATOR . APP_ROOT_DIR);

function __autoload($class_name)
{
	switch ($class_name)
	{
		case 'ViewHelpers':
			require_once 'view/helpers.php';
			break;

		case 'Database':
			require_once 'database/database.php';
			break;

		case 'DatabaseTransaction':
			require_once 'database/transaction.php';
			break;

		case 'Model':
			require_once 'database/model.php';
			break;

		default:
			require_once 'database/model/' . strtolower($class_name) . '.php';
			break;
	}
}

function render_view($view, $environment, $options = null)
{
	$render_content = function() use ($view, $environment)
	{
		extract($environment);
		require("view/$view.html.php");
	};

	$layout = isset($options['layout']) ? $options['layout'] : 'default';
	require("view/layout/$layout.html.php");
}
?>
