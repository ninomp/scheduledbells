<div class="toolbar right-float">
	<a href="editschedule.php?id=<?=$schedule->ScheduleID?>">Edit</a>
</div>
<h2><?=$schedule->ScheduleName?></h2>
<p>Valid from <?=ViewHelpers::format_date($schedule->DateFrom)?> to <?=ViewHelpers::format_date($schedule->DateTo)?></p>
<table>
	<thead>
		<tr>
<?php foreach ($schedule->Days as $day) { ?>
			<th><?=$day->WeekDayName?></th>
<?php } ?>
		</tr>
	</thead>
	<tbody>
		<tr>
<?php foreach ($schedule->Days as $day) { ?>
			<td><?=ViewHelpers::link_to_program($day)?></td>
<?php } ?>
		</tr>
	</tbody>
</table>
<div class="toolbar">
	<a href="editschedule.php?id=<?=$schedule->ScheduleID?>">Edit</a>
</div>
