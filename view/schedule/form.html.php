<h2>Schedule</h2>
<form class="form" method="post">
	<input type="hidden" name="ScheduleID" value="<?=$schedule->ScheduleID?>" />
	<div class="form-row">
		<label class="form-label" for="ScheduleName">Schedule name</label>
		<input type="text" name="ScheduleName" value="<?=$schedule->ScheduleName?>" />
	</div>
	<div class="form-row">
		<label class="form-label" for="DateFrom">Date from</label>
		<input type="date" name="DateFrom" value="<?=$schedule->DateFrom?>" />
	</div>
	<div class="form-row">
		<label class="form-label" for="DateTo">Date to</label>
		<input type="date" name="DateTo" value="<?=$schedule->DateTo?>" />
	</div>
<?php foreach ($schedule->Days as $day) { ?>
	<div class="form-row">
		<label for="Days[]"><?=$day->WeekDayName?></label>
		<?=ViewHelpers::select_for_program('Days[]', $programs, $day->ProgramID)?>
	</div>
<?php } ?>
	<div class="form-row">
		<input type="submit" value="Save" />
	</div>
</form>
