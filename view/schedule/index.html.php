<div class="toolbar right-float">
	<a href="editschedule.php">Create</a>
</div>
<h2>Schedules</h2>
<table>
	<thead>
		<tr>
			<th>Name</th>
			<th>Date range</th>
			<th>Actions</th>
		</tr>
	</thead>
	<tbody>
<?php if (empty($schedules)) { ?>
		<tr>
			<td colspan="3">No data to display !</td>
		</tr>
<?php } else foreach ($schedules as $schedule) { ?>
		<tr>
			<td><a href="schedule.php?id=<?=$schedule->ScheduleID?>"><?=$schedule->ScheduleName?></a></td>
			<td><?=ViewHelpers::format_date($schedule->DateFrom)?> - <?=ViewHelpers::format_date($schedule->DateTo)?></td>
			<td><a href="editschedule.php?id=<?=$schedule->ScheduleID?>">Edit</a></td>
		</tr>
<?php } ?>
	</tbody>
</table>
<div class="toolbar">
	<a href="editschedule.php">Create</a>
</div>
