<?php
final class ViewHelpers
{
	private function __construct() {}
	private function __clone() {}

	public static function format_date($date)
	{
		return date('d.m.Y.', strtotime($date));
	}

	public static function simple_select($select_name, $option_list, $selected_option)
	{
		$s = "<select name=\"$select_name\">";

		foreach ($option_list as $option)
		{
			if ($option == $selected_option)
			{
				$s .= "<option selected>$option</option>";
			}
			else
			{
				$s .= "<option>$option</option>";
			}
		}

		return $s . "</select>\n";
	}

	public static function select_for_program($select_name, $program_list, $selected_program_id)
	{
		$s = "<select name=\"$select_name\"><option value=\"\">None</option>";

		foreach ($program_list as $program)
		{
			if ($program->ProgramID == $selected_program_id)
			{
				$s .= "<option value=\"$program->ProgramID\" selected>$program->ProgramName</option>";
			}
			else
			{
				$s .= "<option value=\"$program->ProgramID\">$program->ProgramName</option>";
			}
		}

		return $s . "</select>\n";
	}

	public static function link_to_program($program)
	{
		if (empty($program->ProgramID))
		{
			return "<em>None</em>";
		}
		else
		{
			return "<a href=\"program.php?id=$program->ProgramID\">$program->ProgramName</a>";
		}
	}
}
?>
