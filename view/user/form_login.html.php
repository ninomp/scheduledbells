<h2>Login</h2>
<form class="form" method="post">
	<div class="form-row">
		<label class="form-label" for="username">Username</label>
		<input type="text" name="username" />
	</div>
	<div class="form-row">
		<label class="form-label" for="password">Password</label>
		<input type="password" name="password" />
	</div>
	<div class="form-row">
		<input type="submit" value="Login" />
	</div>
</form>
