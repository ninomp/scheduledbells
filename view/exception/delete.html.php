<h2>Confirm delete</h2>
<div>
	<p>Are you sure you want to delete exception for <?=ViewHelpers::format_date($exception->Date)?> ?</p>
	<form method="post">
		<input type="hidden" name="date" value="<?=$exception->Date?>">
		<input type="submit" name="choice" value="Yes" />
		<input type="submit" name="choice" value="No" />
	</form>
</div>
