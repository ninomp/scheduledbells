<div class="toolbar right-float">
	<a href="editexception.php">Create</a>
</div>
<h2>Exceptions</h2>
<p>
	<a href="?show=all">All</a>
	<a href="?show=past">Past</a>
	<a href="?show=upcoming">Upcoming</a>
<?php foreach ($years as $year) { ?>
	<a href="?show=<?=$year?>"><?=$year?></a>
<?php } ?>
</p>
<table>
	<thead>
		<tr>
			<th>Date</th>
			<th>Remark</th>
			<th>Program</th>
			<th>Actions</th>
		</tr>
	</thead>
	<tbody>
<?php if (empty($exceptions)) { ?>
		<tr>
			<td colspan="4">No exceptions defined.</td>
		</tr>
<?php } else foreach ($exceptions as $exception) { ?>
		<tr>
			<td><?=ViewHelpers::format_date($exception->Date)?></td>
			<td><?=$exception->Remark?></td>
			<td><?=ViewHelpers::link_to_program($exception)?></td>
			<td>
				<a href="editexception.php?date=<?=$exception->Date?>">Edit</a>
				<a href="deleteexception.php?date=<?=$exception->Date?>">Delete</a>
			</td>
		</tr>
<?php } ?>
	</tbody>
</table>
<div class="toolbar">
	<a href="editexception.php">Create</a>
</div>
