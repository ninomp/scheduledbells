<h2>Exception</h2>
<form class="form" method="post">
	<input type="hidden" name="OriginalDate" value="<?=$exception->OriginalDate?>" />
	<div class="form-row">
		<label class="form-label" for="Date">Date</label>
		<input type="date" name="Date" value="<?=$exception->Date?>" />
	</div>
	<div>
		<label class="form-label" for="Remark">Remark</label>
		<input type="text" name="Remark" value="<?=$exception->Remark?>" />
	</div>
	<div class="form-row">
		<label class="form-label" for="ProgramID">Program</label>
		<?=ViewHelpers::select_for_program('ProgramID', $programs, $exception->ProgramID)?>
	</div>
	<div class="form-row">
		<input type="submit" value="Save" />
	</div>
</form>
