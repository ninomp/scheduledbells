<div class="toolbar right-float">
	<a href="editprogram.php?id=<?=$program->ProgramID?>">Edit</a>
	<a href="editprogramitem.php?program_id=<?=$program->ProgramID?>">Create item</a>
</div>
<h2><?=$program->ProgramName?></h2>
<table>
	<thead>
		<tr>
			<th>Start at</th>
			<th>File</th>
			<th>Actions</th>
		</tr>
	</thead>
	<tbody>
<?php if (empty($items)) { ?>
		<tr>
			<td colspan="3">No data to display !</td>
		</tr>
<?php } else foreach ($items as $item) { ?>
		<tr>
			<td><?=$item->StartTime?></td>
			<td><?=$item->Filename?></td>
			<td>
				<a href="editprogramitem.php?program_id=<?=$program->ProgramID?>&time=<?=$item->StartTime?>">Edit</a>
				<a href="deleteprogramitem.php?program_id=<?=$program->ProgramID?>&time=<?=$item->StartTime?>">Delete</a>
			</td>
		</tr>
<?php } ?>
	</tbody>
</table>
<div class="toolbar">
	<a href="editprogram.php?id=<?=$program->ProgramID?>">Edit</a>
	<a href="editprogramitem.php?program_id=<?=$program->ProgramID?>">Create item</a>
</div>
