<h2>Program item</h2>
<form class="form" method="post">
	<input type="hidden" name="ProgramID" value="<?=$item->ProgramID?>" />
	<input type="hidden" name="OriginalStartTime" value="<?=$item->OriginalStartTime?>" />
	<div class="form-row">
		<label class="form-label" for="StartTime">Start time</label>
		<input type="time" name="StartTime" value="<?=$item->StartTime?>" />
	</div>
	<div class="form-row">
		<label class="form-label" for="Filename">Filename</label>
		<?=ViewHelpers::simple_select("Filename", $audio_files, $item->Filename)?>
	</div>
	<div class="form-row">
		<input type="submit" value="Save" />
	</div>
</form>
