<div class="toolbar right-float">
	<a href="editprogram.php">Create</a>
</div>
<h2>Programs</h2>
<?php if (empty($programs)) { ?>
	<p>No data to display !</p>
<?php } else foreach ($programs as $program) { ?>
	<ul>
		<li>
			<a href="program.php?id=<?=$program->ProgramID?>"><?=$program->ProgramName?></a>
			(<a href="editprogram.php?id=<?=$program->ProgramID?>">Edit</a>)
		</li>
	</ul>
<?php } ?>
<div class="toolbar">
	<a href="editprogram.php">Create</a>
</div>
