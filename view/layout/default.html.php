<!DOCTYPE html>
<html>
<head>
<title>Scheduled Bells</title>
<link rel="stylesheet" type="text/css" href="style.css" />
</head>
<body>
<div id="header">
<h1><a id="logo" href=".">Scheduled Bells</a></h1>
</div>
<div id="menu">
<a href="exceptions.php">Exceptions</a>
<a href="schedules.php">Schedules</a>
<a href="programs.php">Programs</a>
</div>
<div id="content">
<?php $render_content(); ?>
</div>
<div id="footer">Copyright &copy; Progresivne Tehnologije Jelovica 2014.</div>
</body>
</html>
