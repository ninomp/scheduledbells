<?php
final class Database
{
	private function __construct() {}
	private function __clone() {}

	public static function getConnection()
	{
		static $connection = null;

		if ($connection == null)
		{
			require_once 'config/database.php';
			$options = array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION);
			$connection = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD, $options);
		}

		return $connection;
	}

	public static function execute($query, $data = null)
	{
		$db = self::getConnection();
		$statement = $db->prepare($query);
		$statement->execute($data);
	}

	public static function query($query, $data = null)
	{
		$db = self::getConnection();
		$statement = $db->prepare($query);
		$statement->execute($data);
		return $statement;
	}

	public static function lastInsertId($name = null)
	{
		return self::getConnection()->lastInsertId($name);
	}
}
?>
