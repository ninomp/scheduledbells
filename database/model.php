<?php
abstract class Model
{
	function __construct($data_array = null)
	{
		if ($data_array != null)
		{
			foreach ($data_array as $key => $value)
			{
				$this->$key = $value;
			}
		}
	}

	public function __get($name)
	{
		$method_name = 'get' . $name;
		return $this->$method_name();
	}

	public function __set($name, $value)
	{
		$method_name = 'set' . $name;
		$this->$method_name($value);
	}

	public abstract function validate();

	public abstract function save();
}
?>
