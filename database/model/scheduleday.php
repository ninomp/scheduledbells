<?php
class ScheduleDay extends Model
{
	public $ScheduleID;
	public $DayOfWeek;
	public $WeekDayName;
	public $ProgramID;
	public $ProgramName;

	public static function bySchedule($ScheduleID)
	{
		if (empty($ScheduleID))
		{
			$s = Database::query('SELECT DayOfWeek, WeekDayName, NULL AS ProgramID, NULL AS ProgramName FROM weekday');
			return $s->fetchAll(PDO::FETCH_CLASS, 'ScheduleDay');
		}
		else
		{
			$s = Database::query('SELECT * FROM schedule_day NATURAL LEFT JOIN program WHERE ScheduleID = ?', array($ScheduleID));
			return $s->fetchAll(PDO::FETCH_CLASS, 'ScheduleDay');
		}
	}

	public function validate()
	{
		return TRUE;
	}

	public function save()
	{
	}
}
?>
