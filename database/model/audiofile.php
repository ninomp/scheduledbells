<?php
class AudioFile
{
	public static function getDirectoryPath()
	{
		static $path = null;
		if ($path == null)
		{
			require_once 'config/player.php';
			$path = APP_ROOT_DIR . DIRECTORY_SEPARATOR . AUDIO_FILES_ROOT;
		}

		return $path;
	}

	public static function getFilePath($name)
	{
		return self::getDirectoryPath() . $name;
	}

	public static function all()
	{
		$files = scandir(self::getDirectoryPath());
		$filter = function ($filename)
		{
			return preg_match('/^.*\.(wav|ogg|mp3)$/i', $filename);
		};

		return array_filter($files, $filter);
	}
}
?>
