<?php
class ExceptionDay extends Model
{
	public $OriginalDate;
	public $Date;
	public $Remark;
	public $ProgramID;
	public $ProgramName;

	public static function years()
	{
		$s = Database::query('SELECT DISTINCT YEAR(Date) FROM exception');
		return $s->fetchAll(PDO::FETCH_COLUMN, 0);
	}

	public static function all()
	{
		$s = Database::query('SELECT *, Date OriginalDate FROM exception NATURAL LEFT JOIN program ORDER BY Date');
		return $s->fetchAll(PDO::FETCH_CLASS, 'ExceptionDay');
	}

	public static function past()
	{
		$s = Database::query('SELECT *, Date OriginalDate FROM exception NATURAL LEFT JOIN program WHERE Date < CURRENT_DATE() ORDER BY Date');
		return $s->fetchAll(PDO::FETCH_CLASS, 'ExceptionDay');
	}

	public static function upcoming()
	{
		$s = Database::query('SELECT *, Date OriginalDate FROM exception NATURAL LEFT JOIN program WHERE Date >= CURRENT_DATE() ORDER BY Date');
		return $s->fetchAll(PDO::FETCH_CLASS, 'ExceptionDay');
	}

	public static function forYear($year)
	{
		$s = Database::query('SELECT *, Date OriginalDate FROM exception NATURAL LEFT JOIN program WHERE YEAR(Date) = ? ORDER BY Date', array($year));
		return $s->fetchAll(PDO::FETCH_CLASS, 'ExceptionDay');
	}

	public static function forDate($date)
	{
		$s = Database::query('SELECT *, Date OriginalDate FROM exception NATURAL LEFT JOIN program WHERE Date = ?', array($date));
		$object = $s->fetchObject('ExceptionDay');
		return empty($object) ? new ExceptionDay(array('Date' => $date)) : $object;
	}

	public static function delete($date)
	{
		Database::execute("DELETE FROM exception WHERE Date = ?", array($date));
	}

	public function validate()
	{
		return strtotime($this->Date) != FALSE;  // Check is valid date
	}

	public function save()
	{
		if (empty($this->ProgramID))  $this->ProgramID = null;

		if (empty($this->OriginalDate))
		{
			Database::execute("INSERT INTO exception SET Date = ?, Remark = ?, ProgramID = ?", array($this->Date, $this->Remark, $this->ProgramID));
		}
		else
		{
			Database::execute("UPDATE exception SET Date = ?, Remark = ?, ProgramID = ? WHERE Date = ?", array($this->Date, $this->Remark, $this->ProgramID, $this->OriginalDate));
		}
	}
}
?>
