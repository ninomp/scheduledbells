<?php
class Schedule extends Model
{
	public $ScheduleID;
	public $ScheduleName;
	public $DateFrom;
	public $DateTo;

	public static function all()
	{
		$s = Database::query('SELECT * FROM schedule');
		return $s->fetchAll(PDO::FETCH_CLASS, 'Schedule');
	}

	public static function byID($id)
	{
		$s = Database::query('SELECT * FROM schedule WHERE ScheduleID = ?', array($id));
		return $s->fetchObject('Schedule');
	}

	public function getDays()
	{
		if (!isset($this->days))
		{
			$this->days = ScheduleDay::bySchedule($this->ScheduleID);
		}

		return $this->days;
	}

	public function setDays($days)
	{
		$this->days = $days;
	}

	public function validate()
	{
		return TRUE;
	}

	public function save()
	{
		if (empty($this->ScheduleID))
		{
			Database::execute('INSERT INTO schedule (ScheduleName, DateFrom, DateTo) VALUES (?, ?, ?)', array($this->ScheduleName, $this->DateFrom, $this->DateTo));
			$this->ScheduleID = Database::lastInsertId('schedule');
		}
		else
		{
			Database::execute('UPDATE schedule SET ScheduleName = ?, DateFrom = ?, DateTo = ? WHERE ScheduleID = ?', array($this->ScheduleName, $this->DateFrom, $this->DateTo, $this->ScheduleID));
		}

		foreach ($this->days as $day_index => $day_program)
		{
			$day_program_id = empty($day_program) ? null : $day_program;
			Database::execute('CALL SetProgramForScheduleDay(?, ?, ?)', array($this->ScheduleID, $day_index, $day_program_id));
		}
	}
}
?>
