<?php
class Program extends Model
{
	public $ProgramID;
	public $ProgramName;

	public static function all()
	{
		$s = Database::query('SELECT * FROM program');
		return $s->fetchAll(PDO::FETCH_CLASS, 'Program');
	}

	public static function byID($id)
	{
		$s = Database::query('SELECT * FROM program WHERE ProgramID = ?', array($id));
		return $s->fetchObject('Program');
	}

	public function items()
	{
		return ProgramItem::byProgram($this->ProgramID);
	}

	public function validate()
	{
		return trim($this->ProgramName) != '';
	}

	public function save()
	{
		if (empty($this->ProgramID))
		{
			Database::execute('INSERT INTO program SET ProgramName = ?', array($this->ProgramName));
		}
		else
		{
			Database::execute('UPDATE program SET ProgramName = :ProgramName WHERE ProgramID = :ProgramID', (array) $this);
		}
	}
}
?>
