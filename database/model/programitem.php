<?php
class ProgramItem extends Model
{
	public $ProgramID;
	public $OriginalStartTime;
	public $StartTime;
	public $Filename;

	public static function byProgram($program_id)
	{
		$s = Database::query('SELECT *, StartTime OriginalStartTime FROM programitem WHERE ProgramID = ?', array($program_id));
		return $s->fetchAll(PDO::FETCH_CLASS, 'ProgramItem');
	}

	public static function byProgramAndTime($program_id, $time)
	{
		$s = Database::query('SELECT *, StartTime OriginalStartTime FROM programitem WHERE ProgramID = ? AND StartTime = ?', array($program_id, $time));
		return $s->fetchObject('ProgramItem');
	}

	public static function delete($program_id, $time)
	{
		Database::execute('DELETE FROM programitem WHERE ProgramID = ? AND StartTime = ?', array($program_id, $time));
	}

	public function validate()
	{
		return TRUE;
	}

	public function save()
	{
		if (empty($this->OriginalStartTime))
		{
			Database::execute('INSERT INTO programitem (ProgramID, StartTime, Filename) VALUES (?, ?, ?)', array($this->ProgramID, $this->StartTime, $this->Filename));
		}
		else
		{
			Database::execute('UPDATE programitem SET StartTime = :StartTime, Filename = :Filename WHERE ProgramID = :ProgramID AND StartTime = :OriginalStartTime', (array) $this);
		}
	}
}
?>
