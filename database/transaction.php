<?php
class DatabaseTransaction
{
	private $db;

	public function __construct()
	{
		$this->db = Database::getConnection();
		$this->db->beginTransaction();
	}

/*
	public function __destruct()
	{
		$this->db->rollback();
	}
*/

	public function execute($query, $data = null)
	{
		$statement = $this->db->prepare($query);
		$statement->execute($data);
	}

	public function query($query, $data = null)
	{
		$statement = $this->db->prepare($query);
		$statement->execute($data);
		return $statement;
	}

	public function commit()
	{
		$this->db->commit();
	}

	public function rollback()
	{
		$this->db->rollback();
	}

	public function lastInsertId()
	{
		return $this->db->lastInsertId();
	}
}
?>
