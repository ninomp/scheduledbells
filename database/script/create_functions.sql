
DELIMITER .

DROP FUNCTION IF EXISTS GetProgramForDate;

CREATE FUNCTION GetProgramForDate(for_date DATE) RETURNS INT
BEGIN
	DECLARE exception_count INT;
	DECLARE program_id INT;

	SELECT COUNT(*) INTO exception_count
	FROM exception
	WHERE Date = for_date;

	IF exception_count = 1 THEN
		SELECT ProgramID INTO program_id
		FROM exception
		WHERE Date = for_date;
	ELSEIF exception_count = 0 THEN
		SELECT ProgramID INTO program_id
		FROM schedule NATURAL JOIN scheduleday
		WHERE DateFrom <= for_date AND for_date <= DateTo AND DayOfWeek = WEEKDAY(for_date);
	END IF;

	RETURN program_id;
END.

DROP PROCEDURE IF EXISTS SetProgramForScheduleDay;

CREATE PROCEDURE SetProgramForScheduleDay(schedule_id INT, week_day TINYINT, program_id INT)
BEGIN
	IF program_id IS NULL THEN
		DELETE FROM scheduleday WHERE ScheduleID = schedule_id AND DayOfWeek = week_day;
	ELSE
		REPLACE scheduleday (ScheduleID, DayOfWeek, ProgramID) VALUES (schedule_id, week_day, program_id);
	END IF;
END.
