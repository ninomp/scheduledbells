
CREATE OR REPLACE VIEW schedule_day AS
	SELECT ScheduleID, DayOfWeek, WeekDayName, ProgramID
	FROM scheduleday sd NATURAL JOIN weekday wd
UNION
	SELECT ScheduleID, DayOfWeek, WeekDayName, NULL AS ProgramID
	FROM schedule s, weekday wd
	WHERE (ScheduleID, DayOfWeek) NOT IN (SELECT ScheduleID, DayOfWeek FROM scheduleday)
ORDER BY ScheduleID, DayOfWeek
;

CREATE OR REPLACE VIEW schedule_week AS
SELECT
	schedule.ScheduleID,
	schedule.ScheduleName,
	schedule.DateFrom,
	schedule.DateTo,
	mon.ProgramID Monday,
	tue.ProgramID Tuesday,
	wed.ProgramID Wednesday,
	thr.ProgramID Thursday,
	fri.ProgramID Friday,
	sat.ProgramID Saturday,
	sun.ProgramID Sunday
FROM
	schedule
	LEFT JOIN schedule_day mon ON schedule.ScheduleID = mon.ScheduleID AND mon.DayOfWeek = 0
	LEFT JOIN schedule_day tue ON schedule.ScheduleID = tue.ScheduleID AND tue.DayOfWeek = 1
	LEFT JOIN schedule_day wed ON schedule.ScheduleID = wed.ScheduleID AND wed.DayOfWeek = 2
	LEFT JOIN schedule_day thr ON schedule.ScheduleID = thr.ScheduleID AND thr.DayOfWeek = 3
	LEFT JOIN schedule_day fri ON schedule.ScheduleID = fri.ScheduleID AND fri.DayOfWeek = 4
	LEFT JOIN schedule_day sat ON schedule.ScheduleID = sat.ScheduleID AND sat.DayOfWeek = 5
	LEFT JOIN schedule_day sun ON schedule.ScheduleID = sun.ScheduleID AND sun.DayOfWeek = 6
;
