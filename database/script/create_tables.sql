
CREATE TABLE program
(
	ProgramID INT NOT NULL AUTO_INCREMENT,
	ProgramName VARCHAR(55) NOT NULL,
	PRIMARY KEY(ProgramID)
)
ENGINE = InnoDB;

CREATE TABLE programitem
(
	ProgramID INT NOT NULL,
	StartTime TIME NOT NULL,
	Filename VARCHAR(255) NOT NULL,
	PRIMARY KEY(ProgramID, StartTime),
	CONSTRAINT FK_programitem_program FOREIGN KEY (ProgramID) REFERENCES program(ProgramID)
)
ENGINE = InnoDB;

CREATE TABLE weekday
(
	DayOfWeek TINYINT NOT NULL,
	WeekDayName VARCHAR(25) NOT NULL,
	PRIMARY KEy(DayOfWeek)
)
ENGINE = InnoDB;

CREATE TABLE schedule
(
	ScheduleID INT NOT NULL AUTO_INCREMENT,
	ScheduleName VARCHAR(55) NOT NULL,
	DateFrom DATE NOT NULL,
	DateTo DATE NOT NULL,
	PRIMARY KEY(ScheduleID)
)
ENGINE = InnoDB;

CREATE TABLE scheduleday
(
	ScheduleID INT NOT NULL,
	DayOfWeek TINYINT NOT NULL,
	ProgramID INT NOT NULL,
	PRIMARY KEY(ScheduleID, DayOfWeek),
	CONSTRAINT FK_scheduleday_schedule FOREIGN KEY (ScheduleID) REFERENCES schedule(ScheduleID),
	CONSTRAINT FK_scheduleday_weekday FOREIGN KEY (DayOfWeek) REFERENCES weekday(DayOfWeek),
	CONSTRAINT FK_scheduleday_program FOREIGN KEY (ProgramID) REFERENCES program(ProgramID)
)
ENGINE = InnoDB;

CREATE TABLE exception
(
	Date DATE NOT NULL,
	Remark VARCHAR(55) NOT NULL,
	ProgramID INT,
	PRIMARY KEY(Date),
	CONSTRAINT FK_exception_program FOREIGN KEY (ProgramID) REFERENCES program(ProgramID)
)
ENGINE = InnoDB;
