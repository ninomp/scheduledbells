
CREATE DATABASE IF NOT EXISTS bells DEFAULT CHARACTER SET = utf8;

GRANT ALL PRIVILEGES ON bells.* TO 'bells'@'localhost';

USE bells;

SOURCE create_tables.sql;
SOURCE fill_weekdays.sql;
SOURCE create_views.sql;
SOURCE create_functions.sql;
