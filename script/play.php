#!/usr/bin/env php5
<?php
require_once dirname(dirname(__FILE__)) . '/bootstrap.php';
require_once 'config/player.php';

/*
 * This file is intended to be called once a minute i.e every minute by a task scheduler such as cron.
 * For use with cron add "* * * * * bells cd /path/to/this/directory/ && ./play.php" to your '/etc/crontab' file.
 * Note that you need to replace '/path/to/this/directory/' with actual (full) path to this directory.
 * Also note that user called 'bells' must be created beforehand and that it must be given proper privileges to play audio.
 */

$s = Database::query('SELECT Filename FROM programitem WHERE ProgramID = GetProgramForDate(CURDATE()) AND ABS(SUBTIME(StartTime, CURTIME())) < 30 LIMIT 1');
$data = $s->fetchObject();
if ($data)
{
	$player = AUDIO_PLAY_CMD;
	$audio_filename = AudioFile::getFilePath($data->Filename);
	$command = "$player \"$audio_filename\"";
	print_r($command);
	exec($command);
}
?>
