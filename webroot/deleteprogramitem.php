<?php
require_once '../bootstrap.php';

if ($_SERVER['REQUEST_METHOD'] == 'GET')
{
	$item = ProgramItem::byProgramAndTime($_GET['program_id'], $_GET['time']);

	render_view('program/delete_item', array('item' => $item));
}
elseif ($_SERVER['REQUEST_METHOD'] == 'POST')
{
	if ($_POST['choice'] == 'Yes')
	{
		ProgramItem::delete($_POST['program_id'], $_POST['start_time']);
	}

	header('Location: program.php?id=' . $_POST['program_id']);
}
?>
