<?php
require_once '../bootstrap.php';

$programs = Program::all();

render_view('program/index', array('programs' => $programs));
?>
