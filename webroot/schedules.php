<?php
require_once '../bootstrap.php';

$schedules = Schedule::all();

render_view('schedule/index', array('schedules' => $schedules));
?>
