<?php
require_once '../bootstrap.php';

if ($_SERVER['REQUEST_METHOD'] == 'GET')
{
	if (isset($_GET['id']))
	{
		$program = Program::byID($_GET['id']);
	}
	else
	{
		$program = new Program();
	}

	render_view('program/form', array('program' => $program));
}
elseif ($_SERVER['REQUEST_METHOD'] == 'POST')
{
	$program = new Program($_POST);

	if ($program->validate())
	{
		$program->save();

		header("Location: programs.php");
	}
	else
	{
		render_view('program/form', array('program' => $program));
	}
}
?>
