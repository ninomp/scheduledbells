<?php
require_once '../bootstrap.php';

$schedule = Schedule::byID($_GET['id']);

render_view('schedule/show', array('schedule' => $schedule));
?>
