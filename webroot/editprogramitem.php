<?php
require_once '../bootstrap.php';

if ($_SERVER['REQUEST_METHOD'] == 'GET')
{
	if (isset($_GET['program_id']))
	{
		if (isset($_GET['time']))
		{
			$item = ProgramItem::byProgramAndTime($_GET['program_id'], $_GET['time']);
		}
		else
		{
			$item = new ProgramItem();
			$item->ProgramID = $_GET['program_id'];
		}

		render_view('program/form_item', array('item' => $item, 'audio_files' => AudioFile::all()));
	}
}
elseif ($_SERVER['REQUEST_METHOD'] == 'POST')
{
	$item = new ProgramItem($_POST);
	$item->save();

	header('Location: program.php?id=' . $item->ProgramID);
}
?>
