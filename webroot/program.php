<?php
require_once '../bootstrap.php';

$program = Program::byID($_GET['id']);
$items = $program->items();

render_view('program/show', array('program' => $program, 'items' => $items));
?>
