<?php
require_once '../bootstrap.php';

if ($_SERVER['REQUEST_METHOD'] == 'GET')
{
	if (isset($_GET['date']))
	{
		$exceptionday = ExceptionDay::forDate($_GET['date']);
	}
	else
	{
		$exceptionday = new ExceptionDay();
	}

	$programs = Program::all();

	render_view('exception/form', array('exception' => $exceptionday, 'programs' => $programs));
}
elseif ($_SERVER['REQUEST_METHOD'] == 'POST')
{
	$exceptionday = new ExceptionDay($_POST);

	if ($exceptionday->validate())
	{
		$exceptionday->save();

		header("Location: exceptions.php");
	}
	else
	{
		$programs = Program::all();

		render_view('exception/form', array('exception' => $exceptionday, 'programs' => $programs));
	}
}
?>
