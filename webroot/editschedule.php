<?php
require_once '../bootstrap.php';

if ($_SERVER['REQUEST_METHOD'] == 'GET')
{
	if (isset($_GET['id']))
	{
		$schedule = Schedule::byID($_GET['id']);
	}
	else
	{
		$schedule = new Schedule();
	}

	$programs = Program::all();

	render_view('schedule/form', array('schedule' => $schedule, 'programs' => $programs));
}
elseif ($_SERVER['REQUEST_METHOD'] == 'POST')
{
	$schedule = new Schedule($_POST);
	$schedule->save();

	header('Location: schedules.php');
}
?>
