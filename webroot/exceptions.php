<?php
require_once '../bootstrap.php';

$show = isset($_GET['show']) ? $_GET['show'] : 'upcoming';

if (is_numeric($show))
{
	$exceptions = ExceptionDay::forYear($show);
}
elseif ($show == 'all')
{
	$exceptions = ExceptionDay::all();
}
elseif ($show == 'past')
{
	$exceptions = ExceptionDay::past();
}
else
{
	$exceptions = ExceptionDay::upcoming();
}

$years = ExceptionDay::years();

render_view('exception/index', array('exceptions' => $exceptions, 'years' => $years));
?>
