<?php
require_once '../bootstrap.php';

if ($_SERVER['REQUEST_METHOD'] == 'GET')
{
	$exception = ExceptionDay::forDate($_GET['date']);

	render_view('exception/delete', array('exception' => $exception));
}
elseif ($_SERVER['REQUEST_METHOD'] == 'POST')
{
	if ($_POST['choice'] == 'Yes')
	{
		ExceptionDay::delete($_POST['date']);
	}

	header("Location: exceptions.php");
}
?>
